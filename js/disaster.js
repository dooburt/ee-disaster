(function() {
    $(document).ready(function() {
        var Disaster = {
            network: 'ee',
            device: 'mobile',
            networkPanel: $('.network'),
            callingPanel: $('.calling'),
            resultsPanels: $('.result'),
            start: function() {
                this.callingPanel.hide();
                this.resultsPanels.hide();
            },
            bindEvents: function(callback) {
                var self = this;
                $('.network .controls input[type=radio]').on('click', function() {
                    self.network = $(this).val().toString();

                    $('.calling .brand').addClass(self.network);

                    self.networkPanel.fadeOut('fast', function() {
                        self.callingPanel.fadeIn('fast');
                    });

                });

                $('.calling .controls input[type=radio]').on('click', function() {
                    self.device = $(this).val().toString();
                    self.callingPanel.fadeOut('fast', function() {
                        self.renderResultsPanel();
                    });

                });

                $('#startover').on('click', function(e) {
                    e.preventDefault();
                    location.reload();
                });

                if(callback) {
                    callback();
                }
            },
            renderResultsPanel: function() {
                var panel = $('.result.' + this.network + this.device);
                panel.fadeIn('fast');
            }
        };

        Disaster.bindEvents(function() { Disaster.start(); });

    });
})();